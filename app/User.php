<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Carbon\Carbon;

/**
 * Class User
 * @package App
 * @property integer id Идентификатор пользователя.
 * @property string name Имя пользователя.
 * @property Carbon updated_at Дата последнего изменения любого из атрибутов пользователя.
 * @property Carbon created_at Дата, когда пользователь был создан.
 * @property string password Пароль.
 * @property string email Электронная почта пользователя.
 * @property string remember_token Токен для того, чтобы не вводить пароль каждый раз.
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * @param $val string Новый пароль.
     */
    function setPasswordAttribute($val)
    {
        $this->attributes['password'] = bcrypt($val);
    }
}
