@extends('layouts.app')

@section('content')
<div class="row">
    <div class="col-md-12 text-center">
        {{$posts->links()}}
    </div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default">
            <div class="panel-heading">Recent posts</div>

            <ul class="list-group">
                @forelse($posts as $post)
                    <li class="list-group-item">
                        @include('posts.post-for-list', ['post' => $post])
                    </li>
                @empty
                    <li class="list-group-item">
                        <p>No posts :(</p>
                    </li>
                @endforelse
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 text-center">
        {{$posts->links()}}
    </div>
</div>
@endsection
