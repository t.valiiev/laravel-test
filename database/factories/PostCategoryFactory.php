<?php

use Faker\Generator as Faker;

$factory->define(App\PostCategory::class, function (Faker $faker) {
    return [
        'name' => $faker->words(rand(1,2), true),
    ];
});
